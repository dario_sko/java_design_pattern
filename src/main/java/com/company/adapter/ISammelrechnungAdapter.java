package com.company.adapter;

import com.company.ISammelumrechung;
import com.company.IUmrechnen;

public class ISammelrechnungAdapter implements ISammelumrechung {
    private IUmrechnen iUmrechnen;

    public ISammelrechnungAdapter(IUmrechnen iUmrechnen) {
        this.iUmrechnen = iUmrechnen;
    }

    @Override
    public double sammelumrechnen(double[] betraege, String variante) {
        double summe = 0;
        for (int i = 0; i < betraege.length; i++) {
            summe += iUmrechnen.umrechnnen(variante, betraege[i]);
        }
        return summe;
    }
}
