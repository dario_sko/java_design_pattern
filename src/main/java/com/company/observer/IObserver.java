package com.company.observer;

public interface IObserver {
    public void update(String variante, double betrag, double ergebnis);
}
