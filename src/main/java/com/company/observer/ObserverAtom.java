package com.company.observer;

import com.rometools.rome.feed.synd.*;
import com.rometools.rome.io.SyndFeedOutput;

import java.io.FileWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ObserverAtom implements IObserver {
    @Override
    public void update(String variante, double betrag, double ergebnis) {
        createAtomFeed(variante, betrag, ergebnis);
    }

    public String createLogFile(String variante, double betrag, double ergebnis) {
        SimpleDateFormat date = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String ausgangswaehrung = variante.substring(0, 3).toUpperCase();
        String zielwaehrung = variante.substring(5).toUpperCase();
        String log = "\n\nDatum/Zeit: " + date.format(new Date()) +
                "\n Ausgangswährung: " + ausgangswaehrung +
                "\n Ausgangsbetrag: " + betrag +
                "\n Zielwährung: " + zielwaehrung +
                "\n Zielbetrag: " + ergebnis;

        return log;
    }

    // AtomFeed Rome.tools
    public void createAtomFeed(String variante, double betrag, double ergebnis) {
        DateFormat DATE_PARSER = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String feedType = "rss_2.0";
            String fileName = "./logs/atomfeed.xml";

            SyndFeed feed = new SyndFeedImpl();
            feed.setFeedType(feedType);

            feed.setTitle("Sample Feed (created with ROME)");
            feed.setLink("http://rome.dev.java.net");
            feed.setDescription("This feed has been created using ROME (Java syndication utilities");

            List entries = new ArrayList();
            SyndEntry entry;
            SyndContent description;

            entry = new SyndEntryImpl();
            entry.setTitle("Umrechnung");
            entry.setPublishedDate(DATE_PARSER.parse("2004-06-08"));
            description = new SyndContentImpl();
            description.setType("text/html");
            description.setValue("<p>" + createLogFile(variante, betrag, ergebnis) + "</p>");
            entry.setDescription(description);
            entries.add(entry);

            feed.setEntries(entries);

            Writer writer = new FileWriter(fileName, true);
            SyndFeedOutput output = new SyndFeedOutput();
            output.output(feed, writer);
            writer.close();

        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("ERROR: " + ex.getMessage());
        }

    }
}
