package com.company.observer;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ObserverLog implements IObserver {
    @Override
    public void update(String variante, double betrag, double ergebnis) {
        writeIntoFile(createLogFile(variante, betrag, ergebnis));
    }

    public String createLogFile(String variante, double betrag, double ergebnis) {
        SimpleDateFormat date = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String ausgangswaehrung = variante.substring(0, 3).toUpperCase();
        String zielwaehrung = variante.substring(5).toUpperCase();
        String log = "\n\nDatum/Zeit: " + date.format(new Date()) +
                "\n Ausgangswährung: " + ausgangswaehrung +
                "\n Ausgangsbetrag: " + betrag +
                "\n Zielwährung: " + zielwaehrung +
                "\n Zielbetrag: " + ergebnis;

        return log;
    }

    public void writeIntoFile(String log) {
        BufferedWriter ausgabe;
        try {
            ausgabe = new BufferedWriter(
                    new OutputStreamWriter(
                            new FileOutputStream("./logs/ObserverLog.txt", true)));
            ausgabe.write(log);
            ausgabe.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
