package com.company;

import com.company.observer.IObserver;

import java.util.ArrayList;
import java.util.List;

public abstract class WR implements IUmrechnen {

    private IUmrechnen nextWr;
    // Observer
    private List<IObserver> observerList = new ArrayList<>();

    public WR() {

    }

    public WR(IUmrechnen nextWr) {
        this.nextWr = nextWr;
    }

    @Override
    public double umrechnnen(String variante, double betrag) {
        if (this.nextWr != null) {
            double ergebnis = nextWr.umrechnnen(variante, betrag);
            this.notifyObserver(variante, betrag, ergebnis);
            return ergebnis;
        } else {
            System.out.println("Keine Umrechung vorhanden");
            return 0;
        }

    }

    public void setNextWr(IUmrechnen nextWr) {
        this.nextWr = nextWr;
    }

    // Template Hook Pattern
    public final double umrechnen(double betrag) {
        return Math.round((betrag * getFAKTOR()) * 100.0) / 100.0;
    }

    public abstract double getFAKTOR();

    // Observer
    public void notifyObserver(String variante, double betrag, double ergebnis) {
        for (IObserver observer : observerList) {
            observer.update(variante, betrag, ergebnis);
        }
    }

    public void addObserver(IObserver observer) {
        this.observerList.add(observer);
    }

    public void removeObserver(IObserver observer) {
        this.observerList.add(observer);
    }
}
