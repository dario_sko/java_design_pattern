package com.company.decorator;

import com.company.IUmrechnen;

public class WRDecoratorGebuehr05 extends WRDecorator {
    public WRDecoratorGebuehr05(IUmrechnen nextWr) {
        super(nextWr);
    }

    @Override
    public double umrechnnen(String variante, double betrag) {
        return super.umrechnnen(variante, betrag) + (betrag * 0.05);
    }

}
