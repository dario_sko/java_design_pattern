package com.company.decorator;

import com.company.IUmrechnen;

public class WRDecoratorFix5 extends WRDecorator {

    public WRDecoratorFix5() {

    }

    public WRDecoratorFix5(IUmrechnen nextWr) {
        super(nextWr);
    }

    @Override
    public double umrechnnen(String variante, double betrag) {
        return super.umrechnnen(variante, betrag) + 5;
    }

    // Builder Pattern
    public static class Builder {
        private IUmrechnen nextWr;

        public Builder setNextWr(IUmrechnen nextWr) {
            this.nextWr = nextWr;
            return this;
        }

        public WRDecoratorFix5 build() {
            WRDecoratorFix5 wr = new WRDecoratorFix5();
            wr.setNextWr(nextWr);

            return wr;
        }
    }
}
