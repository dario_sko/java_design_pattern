package com.company.decorator;

import com.company.IUmrechnen;
import com.company.WR;

public abstract class WRDecorator extends WR {

    public WRDecorator() {

    }

    public WRDecorator(IUmrechnen nextWr) {
        super(nextWr);
    }

    @Override
    public double umrechnnen(String variante, double betrag) {
        return super.umrechnnen(variante, betrag);
    }

    @Override
    public double getFAKTOR() {
        return 0;
    }
}
