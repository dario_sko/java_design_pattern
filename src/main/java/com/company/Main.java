package com.company;

import com.company.adapter.ISammelrechnungAdapter;
import com.company.chainOfRes.WREurToJpy;
import com.company.chainOfRes.WREurToUsd;
import com.company.decorator.WRDecoratorFix5;
import com.company.observer.ObserverAtom;
import com.company.observer.ObserverLog;

public class Main {

    public static void main(String[] args) {

        WR wr = new WRDecoratorFix5(new WREurToJpy(new WREurToUsd(null)));
        wr.addObserver(new ObserverLog());
        wr.addObserver(new ObserverAtom());

        double betrag = wr.umrechnnen("EurToUsd", 120);
        double betrag2 = wr.umrechnnen("EurToJpy", 120);
        System.out.println("betrag = " + betrag);
        System.out.println("betrag = " + betrag2);

        // Builder
        WR wr1 = new WRDecoratorFix5.Builder()
                .setNextWr(new WREurToJpy.Builder()
                        .setNextWr(new WREurToUsd.Builder()
                                .setNextWr(null)
                                .build()).build()).build();

        wr1.addObserver(new ObserverLog());

        double betrag4 = wr1.umrechnnen("EurToUsd", 120);
        System.out.println("betrag = " + betrag4);


        // Sammelrechnung

        double[] betraege = {100, 150, 200, 250};

        ISammelrechnungAdapter wr2 = new ISammelrechnungAdapter(wr);

        double betrag5 = wr2.sammelumrechnen(betraege, "EurToUsd");
        System.out.println("betrag = " + betrag5);

    }

}
