package com.company.chainOfRes;

import com.company.IUmrechnen;
import com.company.WR;

public class WREurToUsd extends WR {
    private final double FAKTOR = 1.13469;

    public WREurToUsd() {

    }

    public WREurToUsd(IUmrechnen nextWr) {
        super(nextWr);
    }

    @Override
    public double umrechnnen(String variante, double betrag) {
        if (variante.equals("EurToUsd")) {
            return umrechnen(betrag);
        } else {
            return super.umrechnnen(variante, betrag);
        }
    }

    @Override
    public double getFAKTOR() {
        return this.FAKTOR;
    }

    // Builder Pattern
    public static class Builder {
        private IUmrechnen nextWr;

        public Builder setNextWr(IUmrechnen nextWr) {
            this.nextWr = nextWr;
            return this;
        }

        public WREurToUsd build() {
            WREurToUsd wr = new WREurToUsd();
            wr.setNextWr(nextWr);

            return wr;
        }
    }
}
