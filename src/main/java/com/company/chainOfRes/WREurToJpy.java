package com.company.chainOfRes;

import com.company.IUmrechnen;
import com.company.WR;

public class WREurToJpy extends WR {
    private final double FAKTOR = 128.78;

    public WREurToJpy() {

    }

    public WREurToJpy(IUmrechnen nextWr) {
        super(nextWr);
    }

    @Override
    public double umrechnnen(String variante, double betrag) {
        if (variante.equals("EurToJpy")) {
            return umrechnen(betrag);
        } else {
            return super.umrechnnen(variante, betrag);
        }
    }

    @Override
    public double getFAKTOR() {
        return this.FAKTOR;
    }

    // Builder Pattern
    public static class Builder {
        private IUmrechnen nextWr;

        public Builder setNextWr(IUmrechnen nextWr) {
            this.nextWr = nextWr;
            return this;
        }

        public WREurToJpy build() {
            WREurToJpy wr = new WREurToJpy();
            wr.setNextWr(nextWr);

            return wr;
        }
    }

}
