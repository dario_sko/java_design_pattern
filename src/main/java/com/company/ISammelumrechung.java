package com.company;

public interface ISammelumrechung {
    public double sammelumrechnen(double[] betraege, String variante);
}
